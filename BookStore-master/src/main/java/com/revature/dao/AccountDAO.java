package com.revature.dao;

import java.sql.SQLException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import com.revature.entity.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
 
@Transactional
@Repository
public class AccountDAO {
 
    @Autowired
    private SessionFactory sessionFactory;
 
    public Account findAccount(String userName) {
        Session session = this.sessionFactory.getCurrentSession();
        return session.find(Account.class, userName);
    }
    
    public static void main(String[] args) throws SQLException {
		//ProductDAO dao = new ProductDAO();
		AccountDAO acc = new AccountDAO();

		System.out.println(acc.findAccount("manager"));
		
		//System.out.println(dao.findProductInfo("B01"));
}
 
}